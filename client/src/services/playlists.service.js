export const getAllPlaylists = async (token, genres) => {
    try {
        const genresArr = Array.from(genres);
        const res = await fetch(
            `http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/playlists/all${
                genresArr.length ? `?genres=${genresArr.join(",")}` : ""
            }`,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        );

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const getUserPlaylists = async (id, token, pid = null) => {
    try {
        const res = await fetch(
            `http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${id}/playlists${pid ? `?pid=${pid}` : ""}`,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        );
        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const updatePlaylist = async (newName, userId, playlistId, token) => {
    try {
        const res = await fetch(
            `http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${userId}/playlists/${playlistId}`,
            {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({ name: newName }),
            }
        );

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const createPlaylist = async (name, userId, playtime, trackList, genres, token) => {
    try {
        const trackIds = trackList.map((track) => track.trackDeezerId).join(",");
        const genreIds = genres.map((genre) => genre.deezerId).join(",");

        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${userId}/playlists`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify({ name, tracks: trackIds, playtime, genres: genreIds }),
        });

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const deletePlaylist = async (uid, pid, token) => {
    try {
        const res = await fetch(
            `http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${uid}/playlists/${pid}`,
            {
                method: "DELETE",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        );

        return await res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const getRandomImage = async (searchKeyword) => {
    try {
        const randomPage = Math.floor(Math.random() * 10) + 1;
        const imagesPerPage = 50;
        const res = await fetch(
            `https://pixabay.com/api/?key=${process.env.REACT_APP_PIXABEY_API}&q=${searchKeyword}&page=${randomPage}&per_page=${imagesPerPage}`
        );
        const images = res.json().hits;
        const randomIndex = Math.floor(Math.random() * images.length);

        return images[randomIndex];
    } catch (err) {
        console.warn(err.message);
    }
};

export const getTrackData = async (id, token) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/tracks/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const getTracksByGenre = async (genre, token) => {
    try {
        const res = await fetch(
            `http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/tracks?genre=${genre}&pageSize=1000`,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        );

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const getAllTracks = async (token) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/tracks?pageSize=5000`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const getLocationSuggestions = async (q) => {
    try {
        const query = q.split(" ").join("%20");

        if (query.length > 3) {
            const res = await fetch(
                `http://dev.virtualearth.net/REST/v1/Autosuggest?query=${query}&userCircularMapView=42.7339,25.4858,1000000&maxResults=3&includeEntityTypes=Place,Address&key=${process.env.REACT_APP_BING_KEY}`
            );
            const data = res.json();
            return data.resourceSets[0].resources[0].value;
        }
    } catch (err) {
        console.warn(err.message);
    }
};

export const getTripDuration = async (x, y) => {
    try {
        const pointA = x
            .split(", ")
            .map((val) => val.split(" ").join("%20"))
            .join("%2C");
        const pointB = y
            .split(", ")
            .map((val) => val.split(" ").join("%20"))
            .join("%2C");

        const res = await fetch(
            `http://dev.virtualearth.net/REST/V1/Routes/Driving?wp.0=${pointA}&wp.1=${pointB}&avoid=minimizeTolls&key=${process.env.REACT_APP_BING_KEY}`
        );
        const data = res.json();

        return data.resourceSets[0]?.resources[0]?.travelDurationTraffic || null;
    } catch (err) {
        console.warn(err.message);
    }
};

export const getGenresInfo = async (token) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/tracks/genres`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        const genresData = res.json();
        return genresData;
    } catch (err) {
        console.warn(err.message);
    }
};
