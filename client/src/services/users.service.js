export const registerUser = async ({ email, username, password }) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/register`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ email, username, password }),
        });

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const loginUser = async (loginDetails) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(loginDetails),
        });

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const logoutUser = async (token) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/logout`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
        });
        const logout = res.json();

        return logout.message.includes("successful");
    } catch (err) {
        console.warn(err.message);
    }
};

export const getUserData = async (id, token) => {
    try {
        const user = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return await user.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const uploadAvatar = async (id, token, data) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users/${id}/uploads`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
            },
            body: data,
        });
        const updatedUser = res.json();
        return updatedUser.avatar || null;
    } catch (err) {
        console.warn(err.message);
    }
};

export const getAllUsers = async (token) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/admin/users`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const searchAllUsers = async (keyword, token) => {
    try {
        const res = await fetch(
            `http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/admin/users?username=${keyword}`,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        );

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const banUser = async (id, token) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/admin/users/banstatus/${id}`, {
            method: "PUT",
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        return res.json();
    } catch (err) {
        console.warn(err.message);
    }
};

export const updateUser = async (id, token, data) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/admin/users/${id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify(data),
        });
        const updatedUser = res.json();
        return updatedUser || null;
    } catch (err) {
        console.warn(err.message);
    }
};

export const updateUserPassword = async (token, password) => {
    try {
        const res = await fetch(`http://localhost:${process.env.REACT_APP_BEPORT}/v1/api/users`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            body: JSON.stringify({ password }),
        });
        const updatedUser = res.json();

        return updatedUser || null;
    } catch (err) {
        console.warn(err.message);
    }
};

export const logout = (token, auth) => {
	logoutUser(token);
	localStorage.removeItem('token');
	auth.setAuthState({
		user: null,
		isLoggedIn: false,
	});
};
