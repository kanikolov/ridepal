import { useState, useEffect } from 'react';

const useAudio = (setLoadTrack) => {
	const [audio, setAudio] = useState(null);
	const [playing, setPlaying] = useState(false);
	const [error, setError] = useState(false);

	const toggle = () => setPlaying(!playing);
	useEffect(() => {
		if (audio && !audio.error) {
			playing ? audio.play() : audio.pause();
		} else if (audio?.error) {
			setError(true);

			setTimeout(() => {
				setLoadTrack(false);
				setError(false);
			}, 3000);
		}
	}, [playing]);

	useEffect(() => {
		if (audio) {
			audio.addEventListener('ended', () => setPlaying(false));
			return () => {
				audio.removeEventListener('ended', () => setPlaying(false));
			};
		}
	}, [audio]);

	return [playing, toggle, setAudio, error];
};

export default useAudio;
