import { Button, Dialog, DialogTitle, makeStyles, MenuItem, Select, TextField } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { getToken, getUser } from "../../../providers/authentication";
import { banUser, updateUser } from "../../../services/users.service.js";
import "./UserUpdate.css";

const useStyles = makeStyles(() => ({
    MuiGridFieldDark: {
        backgroundColor: "rgba(0, 0, 0, 0.1)",
    },
    MuiGridFieldLight: {
        backgroundColor: "rgba(255, 255, 255, 0.95)",
    },
}));

const UserUpdate = ({ open, handleClose, userData, updateUsersEditsCounter, darkTheme }) => {
    const token = getToken();
    const user = getUser();
    const { handleSubmit, control, reset } = useForm();
    const [msg, setMsg] = useState("");

    useEffect(() => {
        setTimeout(() => {
            setMsg("");
        }, 1000);
        reset(userData);
    }, [userData]);

    const classes = useStyles();

    const update = async (id, userData) => {
        if (userData) {
            const req = await updateUser(id, token, userData);
            if (req.message) setMsg(req.message);
            else setMsg("Update successful!");
            updateUsersEditsCounter();
        }
    };

    return (
        user?.role === "Admin" && (
            <form className='update-user-form' onSubmit={(e) => e.preventDefault()}>
                <div className='edit-user-wrapper'>
                    <Dialog open={open} onClose={handleClose}>
                        <div className='edit-user'>
                            <DialogTitle>
                                <div className='update-header'>Update user</div>
                            </DialogTitle>
                            <div className='edit-user-body'>
                                <div className='update-message'>
                                    <p className={msg.includes("successful") ? "success-msg" : "error-msg"}>{msg}</p>
                                </div>
                                <div className='edit-user-item'>
                                    <Controller
                                        name='username'
                                        control={control}
                                        rules={{
                                            minLength: {
                                                value: 3,
                                                message: "Username must be at least 3 characters",
                                            },
                                            maxLength: {
                                                value: 100,
                                                message: "Username must be no more than 100 characters",
                                            },
                                        }}
                                        render={({ field: { onChange, value }, fieldState: { error } }) => (
                                            <>
                                                <p className='item-label'>Change username to:</p>
                                                <TextField
                                                    fullWidth={true}
                                                    color='secondary'
                                                    value={value}
                                                    label={userData.username}
                                                    variant='filled'
                                                    onChange={onChange}
                                                    error={!!error}
                                                    helperText={error ? error.message : null}
                                                    type='text'
                                                    className={
                                                        darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight
                                                    }
                                                />
                                            </>
                                        )}
                                    />
                                </div>
                                <div className='edit-user-item'>
                                    {userData.id === user.id ? (
                                        <></>
                                    ) : (
                                        <div className='edit-user-row'>
                                            <Controller
                                                name='role'
                                                control={control}
                                                render={({ field: { onChange, value } }) => (
                                                    <>
                                                        Change role to: &nbsp;
                                                        <Select
                                                            onChange={onChange}
                                                            value={value}
                                                            key={value}
                                                            defaultValue={userData.role}
                                                        >
                                                            <MenuItem
                                                                label='Select a role'
                                                                value={"Admin"}
                                                                key={"Admin"}
                                                                name={"Admin"}
                                                            >
                                                                Admin
                                                            </MenuItem>
                                                            <MenuItem
                                                                label='Select a role'
                                                                value={"User"}
                                                                key={"User"}
                                                                name={"User"}
                                                            >
                                                                User
                                                            </MenuItem>
                                                        </Select>
                                                    </>
                                                )}
                                            />
                                        </div>
                                    )}
                                </div>

                                <div className='actions'>
                                    <Button
                                        onClick={handleSubmit((formData) => update(userData.id, formData))}
                                        color={darkTheme ? "primary" : "secondary"}
                                        variant='contained'
                                        type='submit'
                                    >
                                        Submit
                                    </Button>

                                    {userData.id === user.id ? (
                                        <></>
                                    ) : (
                                        <>
                                            <Button
                                                color={darkTheme ? "primary" : "secondary"}
                                                variant='contained'
                                                onClick={() => {
                                                    banUser(userData.id, token);
                                                    updateUsersEditsCounter();
                                                }}
                                            >
                                                {userData.banExpiry === "not banned" ? "Ban user" : "Unban user"}
                                            </Button>
                                        </>
                                    )}
                                    <Link to={`/profile/${userData.id}`}>
                                        <Button color={darkTheme ? "primary" : "secondary"} variant='contained'>
                                            View profile
                                        </Button>
                                    </Link>

                                    <Button
                                        onClick={() => handleClose()}
                                        color={darkTheme ? "secondary" : "primary"}
                                        variant='contained'
                                    >
                                        Close
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </Dialog>
                </div>
            </form>
        )
    );
};

export default UserUpdate;
