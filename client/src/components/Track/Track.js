import { useEffect, useState } from "react";
import useAudio from "../../hooks/useAudio.js";
import convertTime from "../../helpers/timeConverter.js";
import { makeStyles } from "@material-ui/core/styles";
import { Alert } from "@material-ui/lab";
import { AiFillPlayCircle, AiFillPauseCircle } from "react-icons/ai";
import { Bars } from "@agney/react-loading";
import "./Track.css";
import { getTrackData } from "../../services/playlists.service.js";
import { getToken } from "../../providers/authentication.js";

const Track = ({ id }) => {
    const [track, setTrack] = useState(null);
    const [loadTrack, setLoadTrack] = useState(false);
    const [playing, toggle, setAudio, error] = useAudio(setLoadTrack);
    const [loader, setLoader] = useState(false);
    const playtime = track && convertTime(track.duration);
    const token = getToken();

    const useStyles = makeStyles((theme) => ({
        root: {
            width: "100%",
            "& > * + *": {
                marginTop: theme.spacing(2),
            },
        },
    }));

    const classes = useStyles();

    const toggleLoad = () => {
        setLoader(true);
        setAudio(new Audio(track.previewUrl));
        setTimeout(() => {
            setLoader(false);
            setLoadTrack(true);
            toggle();
        }, Math.floor(Math.random() * 2000));
    };

    useEffect(() => {
        (async () => {
            const track = await getTrackData(id, token);
            setTrack(track);
        })();
    }, []);

    return track ? (
        <>
            <div className='track'>
                {!loadTrack ? (
                    <span onClick={toggleLoad}>
                        {loader ? <Bars className='track-loader' /> : <AiFillPlayCircle className='play-icon' />}
                    </span>
                ) : (
                    <span onClick={toggle}>
                        {playing ? (
                            <AiFillPauseCircle className='play-icon' />
                        ) : (
                            <AiFillPlayCircle className='play-icon' />
                        )}
                    </span>
                )}

                <p className='track-title'>{track.title}</p>
                <p>{track.artistName}</p>
                <span>{track.genreTitle}</span>
                <span>{playtime}</span>
            </div>
            {error && (
                <div className={classes.root}>
                    <Alert severity='info'>There was an error playing this track</Alert>
                </div>
            )}
        </>
    ) : (
        <h3>Loading...</h3>
    );
};

export default Track;
