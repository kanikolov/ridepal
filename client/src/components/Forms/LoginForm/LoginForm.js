import React, { useForm, Controller } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { Grid, makeStyles, TextField } from '@material-ui/core';
import { Button } from '@material-ui/core';

const LoginForm = ({ loginHandler, msg, darkTheme }) => {
	const { handleSubmit, control } = useForm();

	const useStyles = makeStyles(() => ({
		MuiGrid: {
			width: '100%',
		},
		MuiGridFieldDark: {
			backgroundColor: 'rgba(0, 0, 0, 0.1)',
		},
		MuiGridFieldLight: {
			backgroundColor: 'rgba(255, 255, 255, 0.95)',
		},
		MuiGridMsg: {
			width: '100%',
		},
		MuiGridBtn: {
			width: '100%',
		},
		MuiGridLink: {
			width: '100%',
			textAlign: 'center',
		},
	}));

	const classes = useStyles();

	return (
		<form className='login-form' onSubmit={(e) => e.preventDefault()}>
			<Grid container direction='column' justify='center' alignItems='center' spacing={3}>
				<Grid item xs={12} className={classes.MuiGridMsg}>
					<p style={{ textAlign: 'center' }}>{msg}</p>
				</Grid>
				<Grid item xs={12} className={classes.MuiGrid}>
					<Controller
						name='email'
						control={control}
						defaultValue=''
						rules={{
							required: 'Email is required',
							pattern: { value: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/, message: 'Provide a valid email address' },
						}}
						render={({ field: { onChange, value }, fieldState: { error } }) => (
							<TextField
								fullWidth={true}
								color='secondary'
								label='Email'
								variant='filled'
								value={value}
								onChange={onChange}
								error={!!error}
								helperText={error ? error.message : null}
								type='email'
								className={darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} className={classes.MuiGrid}>
					<Controller
						name='password'
						control={control}
						defaultValue=''
						rules={{
							required: 'Password is required',
						}}
						render={({ field: { onChange, value }, fieldState: { error } }) => (
							<TextField
								fullWidth={true}
								color='secondary'
								label='Password'
								variant='filled'
								value={value}
								onChange={onChange}
								error={!!error}
								helperText={error ? error.message : null}
								type='password'
								className={darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} className={classes.MuiGridBtn}>
					<Button fullWidth={true} type='submit' variant='contained' color='primary' onClick={handleSubmit((formData) => loginHandler(formData))}>
						Login
					</Button>
				</Grid>
				<Grid item xs={12} className={classes.MuiGridLink}>
					<Link to='/register'>Don't have an account yet? Sign up</Link>
				</Grid>
			</Grid>
		</form>
	);
};

export default LoginForm;
