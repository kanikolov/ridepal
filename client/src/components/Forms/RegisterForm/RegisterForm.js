import { Link } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { useRef } from 'react';
import { Grid, makeStyles, TextField } from '@material-ui/core';
import { Button } from '@material-ui/core';

const RegisterForm = ({ registerHandler, darkTheme, msg }) => {
	const { handleSubmit, control, watch } = useForm();
	const password = useRef({});
	password.current = watch('password', '');

	const useStyles = makeStyles(() => ({
		MuiGrid: {
			width: '100%',
		},
		MuiGridFieldDark: {
			backgroundColor: 'rgba(0, 0, 0, 0.1)',
		},
		MuiGridFieldLight: {
			backgroundColor: 'rgba(255, 255, 255, 0.95)',
		},
		MuiGridMsg: {
			width: '100%',
		},
		MuiGridBtn: {
			width: '100%',
		},
		MuiGridLink: {
			width: '100%',
			textAlign: 'center',
		},
	}));

	const classes = useStyles();

	return (
		<form className='register-form' onSubmit={(e) => e.preventDefault()}>
			<Grid container direction='column' justify='center' alignItems='center' spacing={3}>
				<Grid item xs={12} className={classes.MuiGridMsg}>
					<p style={{ textAlign: 'center' }}>{msg}</p>
				</Grid>
				<Grid item xs={12} className={classes.MuiGrid}>
					<Controller
						name='email'
						control={control}
						defaultValue=''
						rules={{
							required: 'Email is required',
							pattern: { value: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/, message: 'Provide a valid email address' },
							minLength: {
								value: 5,
								message: 'Provide a valid email address',
							},
							maxLength: {
								value: 100,
								message: 'Email must be no more than 100 characters',
							},
						}}
						render={({ field: { onChange, value }, fieldState: { error } }) => (
							<TextField
								fullWidth={true}
								label='Email'
								color='secondary'
								variant='filled'
								value={value}
								onChange={onChange}
								error={!!error}
								helperText={error ? error.message : null}
								type='email'
								className={darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} className={classes.MuiGrid}>
					<Controller
						name='username'
						control={control}
						defaultValue=''
						rules={{
							required: 'Username is required',
							minLength: {
								value: 3,
								message: 'Username must be at least 3 characters',
							},
							maxLength: {
								value: 100,
								message: 'Username must be no more than 100 characters',
							},
						}}
						render={({ field: { onChange, value }, fieldState: { error } }) => (
							<TextField
								fullWidth={true}
								color='secondary'
								label='Username'
								variant='filled'
								value={value}
								onChange={onChange}
								error={!!error}
								helperText={error ? error.message : null}
								type='text'
								className={darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} className={classes.MuiGrid}>
					<Controller
						name='password'
						control={control}
						defaultValue=''
						rules={{
							required: 'Password is required',
							minLength: {
								value: 8,
								message: 'Password must have at least 8 characters',
							},
							maxLength: {
								value: 128,
								message: 'Password must be no more than 128 characters',
							},
						}}
						render={({ field: { onChange, value }, fieldState: { error } }) => (
							<TextField
								color='secondary'
								fullWidth={true}
								label='Password'
								variant='filled'
								value={value}
								onChange={onChange}
								error={!!error}
								helperText={error ? error.message : null}
								type='password'
								className={darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} className={classes.MuiGrid}>
					<Controller
						name='rePassword'
						control={control}
						defaultValue=''
						rules={{
							validate: (value) => value === password.current || 'The passwords do not match',
						}}
						render={({ field: { onChange, value }, fieldState: { error } }) => (
							<TextField
								fullWidth={true}
								color='secondary'
								label='Repeat Password'
								variant='filled'
								value={value}
								onChange={onChange}
								error={!!error}
								helperText={error ? error.message : null}
								type='password'
								className={darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight}
							/>
						)}
					/>
				</Grid>
				<Grid item xs={12} className={classes.MuiGridBtn}>
					<Button fullWidth={true} type='submit' variant='contained' color='primary' onClick={handleSubmit((formData) => registerHandler(formData))}>
						Register
					</Button>
				</Grid>
				<Grid item xs={12} className={classes.MuiGridLink}>
					<Link to='/login'>Already have an account? Sign in</Link>
				</Grid>
			</Grid>
		</form>
	);
};

export default RegisterForm;
