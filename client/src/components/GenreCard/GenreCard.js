import { useState } from 'react';
import './GenreCard.css';

const GenreCard = ({ genre, setGenre, selected }) => {
	const [clicked, setClicked] = useState(Boolean(selected.find((g) => g.name === genre.name)));

	const toggle = () => {
		if (clicked) {
			setGenre((prev) => prev.filter((g) => g.name !== genre.name));
		} else {
			setGenre((prev) => [...prev, genre]);
		}

		setClicked(!clicked);
	};

	return (
		<div className={`genre-card${clicked ? '-active' : ''}${selected.length >= 5 && !clicked ? '-disabled' : ''}`} onClick={toggle}>
			<img src={genre.pictureBig} alt={genre.name} />
			<span className='genre-name'>{genre.name}</span>
		</div>
	);
};

export default GenreCard;
