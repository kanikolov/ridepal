import React from "react";

const ProgressBar = ({ trackProgress, duration, onScrub, onScrubEnd }) => {
    const currentPercentage = duration ? `${(trackProgress / duration) * 100}%` : "0%";
    const trackStyling = `
      -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${currentPercentage}, #fff), color-stop(${currentPercentage}, #777))
    `;

    return (
        <div className='progress-bar-container'>
            <input
                type='range'
                value={trackProgress}
                step='1'
                min='0'
                max={duration ? duration : `${duration}`}
                className='progress'
                onChange={(e) => onScrub(e.target.value)}
                onMouseUp={onScrubEnd}
                onKeyUp={onScrubEnd}
                style={{ background: trackStyling }}
            />
        </div>
    );
}

export default ProgressBar;
