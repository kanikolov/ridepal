import PlayerDetails from './PlayerDetails';
import PlayerControls from './PlayerControls';
import './Player.css';
import Footer from '../Footer/Footer';
import { useEffect, useRef, useState } from 'react';
import ProgressBar from './ProgressBar';
import Button from '../Button/Button';

const Player = ({ currentSongIndex, setCurrentSongIndex, nextSongIndex, songs, setPlayer, darkTheme }) => {
	const [playing, setPlaying] = useState(false);
	const [trackProgress, setTrackProgress] = useState(0);

	const audioEl = useRef(new Audio(songs[currentSongIndex].previewUrl));
	const intervalRef = useRef();
	const isReady = useRef(false);

	const startTimer = () => {
		// Clear any timers already running
		clearInterval(intervalRef.current);

		intervalRef.current = setInterval(() => {
			if (audioEl.current?.ended) {
				skipSong();
			} else {
				setTrackProgress(audioEl.current.currentTime);
			}
		}, [1000]);
	};

	const onScrub = (value) => {
		clearInterval(intervalRef.current);
		audioEl.current.currentTime = value;
		setTrackProgress(audioEl.current.currentTime);
	};

	const onScrubEnd = () => {
		if (!playing) {
			setPlaying(true);
		}
		startTimer();
	};

	const closePlayer = () => {
		audioEl.current.pause();
		setCurrentSongIndex(0);
		setPlayer(false);
	};

	useEffect(() => {
		if (playing) {
			audioEl.current.play();
			startTimer();
		} else {
			clearInterval(intervalRef.current);
			audioEl.current.pause();
		}
	}, [playing]);

	useEffect(() => {
		return () => {
			audioEl.current.pause();
			clearInterval(intervalRef.current);
		};
	}, []);

	useEffect(() => {
		audioEl.current.pause();

		audioEl.current = new Audio(songs[currentSongIndex].previewUrl);
		setTrackProgress(audioEl.current.currentTime);

		if (isReady.current) {
			audioEl.current.play();
			setPlaying(true);
			startTimer();
		} else {
			isReady.current = true;
		}
	}, [currentSongIndex]);

	const skipSong = (forwards = true) => {
		if (forwards) {
			setCurrentSongIndex(() => {
				let temp = currentSongIndex;
				temp++;

				if (temp > songs.length - 1) {
					temp = 0;
				}

				return temp;
			});
		} else {
			setCurrentSongIndex(() => {
				let temp = currentSongIndex;
				temp--;

				if (temp < 0) {
					temp = songs.length - 1;
				}

				return temp;
			});
		}
	};

	return (
		<>
			<div className='c-player'>
				<div className='now-playing-box'>
					<Button text='BACK TO PLAYLIST' handleClick={closePlayer} />
					<h4>Now playing</h4>
				</div>
				<PlayerDetails song={songs[currentSongIndex]} />
				<ProgressBar trackProgress={trackProgress} duration={audioEl.current.duration} onScrub={onScrub} onScrubEnd={onScrubEnd} />
				<PlayerControls playing={playing} setPlaying={setPlaying} skipSong={skipSong} darkTheme={darkTheme} />
				<p>
					<strong>Next up:</strong> {songs[nextSongIndex].title} by {songs[nextSongIndex].artistName}
				</p>
			</div>
			<Footer />
		</>
	);
};

export default Player;
