import { FaPlay, FaPause, FaForward, FaBackward } from "react-icons/fa";
import React from "react";

const PlayerControls = ({ playing, setPlaying, skipSong, darkTheme }) => {
    return (
        <div className='c-player-controls'>
            <button className='skip-btn' onClick={() => skipSong(false)}>
                <FaBackward />
            </button>
            <button className={`play-btn${darkTheme ? "-dark" : ""}`} onClick={() => setPlaying(!playing)}>
                {playing ? <FaPause /> : <FaPlay />}
            </button>
            <button className='skip-btn' onClick={() => skipSong()}>
                <FaForward />
            </button>
        </div>
    );
};

export default PlayerControls;
