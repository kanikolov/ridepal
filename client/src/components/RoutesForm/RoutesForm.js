import { Grid, makeStyles, TextField } from "@material-ui/core";
import { useEffect, useState } from "react";
import { getLocationSuggestions } from "../../services/playlists.service.js";
import Suggestions from "../SuggestionsBox/Suggestions.js";

const RoutesForm = ({ startLocation, setStartLocation, endLocation, setEndLocation, errors, setErrors, darkTheme }) => {
    const [startQuery, setStartQuery] = useState("");
    const [endQuery, setEndQuery] = useState("");
    const [startSuggestions, setStartSuggestions] = useState([]);
    const [endSuggestions, setEndSuggestions] = useState([]);
    const useStyles = makeStyles(() => ({
        MuiGrid: {
            width: "100%",
        },
        MuiGridFieldDark: {
            backgroundColor: "rgba(0, 0, 0, 0.1)",
        },
        MuiGridFieldLight: {
            backgroundColor: "rgba(255, 255, 255, 0.7)",
        },
        MuiGridMsg: {
            width: "100%",
        },
        MuiGridBtn: {
            width: "100%",
        },
        MuiGridLink: {
            width: "100%",
            textAlign: "center",
        },
    }));

    useEffect(() => {
        (async () => {
            const data = await getLocationSuggestions(startQuery);
            setStartSuggestions(data);
        })();
    }, [startQuery]);

    useEffect(() => {
        (async () => {
            const data = await getLocationSuggestions(endQuery);
            setEndSuggestions(data);
        })();
    }, [endQuery]);

    const classes = useStyles();

    return (
        <form className='routes-form' onSubmit={(e) => e.preventDefault()}>
            <Grid container direction='column' justify='center' alignItems='center' spacing={3}>
                <Grid item xs={12} className={classes.MuiGrid}>
                    <TextField
                        fullWidth={true}
                        label='Starting location'
                        variant='filled'
                        value={startLocation}
                        onChange={(e) => {
                            setStartLocation(e.target.value);
                            setStartQuery(e.target.value);
                            setErrors({ ...errors, start: !e.target.value });
                        }}
                        error={errors.start}
                        helperText={errors.start && "required"}
                        type='text'
                        className={darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight}
                    />
                    {startSuggestions && (
                        <Suggestions
                            suggestions={startSuggestions}
                            setLocation={setStartLocation}
                            resetSuggestions={setStartSuggestions}
                        />
                    )}
                </Grid>
                <Grid item xs={12} className={classes.MuiGrid}>
                    <TextField
                        fullWidth={true}
                        label='Final location'
                        variant='filled'
                        value={endLocation}
                        onChange={(e) => {
                            setEndLocation(e.target.value);
                            setEndQuery(e.target.value);
                            setErrors({ ...errors, end: !e.target.value });
                        }}
                        error={errors.end}
                        helperText={errors.end && "required"}
                        type='text'
                        className={darkTheme ? classes.MuiGridFieldDark : classes.MuiGridFieldLight}
                    />
                    {endSuggestions && (
                        <Suggestions
                            suggestions={endSuggestions}
                            setLocation={setEndLocation}
                            resetSuggestions={setEndSuggestions}
                        />
                    )}
                </Grid>
            </Grid>
        </form>
    );
};

export default RoutesForm;
