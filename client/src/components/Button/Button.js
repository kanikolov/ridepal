import { motion } from 'framer-motion';

const Button = ({ text, handleClick }) => {
	const startBtnVariants = {
		initial: {
			opacity: 0,
		},
		transition: {
			duration: 1,
		},
		animate: {
			opacity: 1,
		},
		hover: {
			scale: 1.05,
			textShadow: '0px 0px 4px #e1f3f4',
			transition: {
				duration: 0.4,
			},
		},
	};

	return (
		<motion.h2
			style={{ fontSize: '1.2rem', border: '2px white solid', borderRadius: '50px', padding: '10px 20px', minWidth: '100px', textAlign: 'center' }}
			variants={startBtnVariants}
			whileHover='hover'
			initial='initial'
			transition='transition'
			animate='animate'
			className='start-button'
			onClick={handleClick}
		>
			{text}
		</motion.h2>
	);
};

export default Button;
