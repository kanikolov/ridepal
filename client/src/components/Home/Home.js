import { getUser } from '../../providers/authentication.js';
import './Home.css';
import Footer from '../Footer/Footer';
import GuestHome from '../GuestHome/GuestHome.js';
import UserHome from '../UserHome/UserHome.js';

const Home = ({ darkTheme }) => {
	const user = getUser();
	return (
		<>
			<div className='home-wrapper'>{user ? <UserHome /> : <GuestHome darkTheme={darkTheme} />}</div>
			<Footer darkTheme={darkTheme} />
		</>
	);
};

export default Home;
