import express from 'express';
import transformBody from '../middlewares/transform-body.js';
import validateUpdateBody from '../middlewares/validate-update-body.js';
import { getPlaylist, updatePlaylist } from '../services/admin.service.js';
import updatePlaylistValidator from '../validators/update-playlist.schema.js';

const adminPlaylistsController = express.Router();

adminPlaylistsController

	// update a playlist by id
	.put('/playlists/:pid', transformBody(updatePlaylistValidator), validateUpdateBody('playlists', updatePlaylistValidator), async (req, res) => {
		const playlistId = +req.params.pid;

		const playlist = await getPlaylist(playlistId);

		if (!playlist) return res.status(404).send({ message: 'Playlist not found.' });
		else {
			const updated = await updatePlaylist(playlistId, req.body);

			if (!updated) return res.status(404).send({ message: 'Something went wrong... Please try again later.' });
			else return res.status(200).send({ message: `Success: playlist ${playlistId} was updated! ` });
		}
	})

	// delete a playlist by id
	.delete('/playlists/:pid', async (req, res) => {
		const playlistId = +req.params.pid;

		const playlist = await getPlaylist(playlistId);

		if (!playlist) return res.status(404).send({ message: 'Playlist not found.' });
		else {
			const deleted = await deleteUser(playlistId);

			if (!deleted) return res.status(404).send({ message: 'Something went wrong... Please try again later.' });
			else return res.status(200).send({ message: `Success: playlist with id ${playlistId} was deleted!` });
		}
	});

export default adminPlaylistsController;
