export default {
	name: (value) => typeof value === 'string' && value.length > 2 && value.length <= 100,
};