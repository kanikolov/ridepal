import validator from 'validator';

export default {
	username: (value) => typeof value === 'string' && value.length >= 3 && value.length <= 100 && value.split(' ').length === 1,
	password: (value) => typeof value === 'string' && value.length >= 8 && value.length <= 128,
	email: (value) => typeof value === 'string' && value.length >= 5 && value.length <= 100 && validator.isEmail(value),
};
