export default {
	name: (value) => typeof value === 'string' && value.length > 2 && value.length <= 100,
    tracks: (value) => typeof value === 'string' && value.length > 0,
    playtime: (value) => typeof value === 'number' && value > 0,
    genres: (value) => typeof value === 'string' && value.length > 0,
    poster: (value) => typeof value === 'string' && value.length > 0
};