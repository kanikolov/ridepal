import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './authentication/strategy.js';
import apiVersion from './middlewares/api-version.js';
import authController from './controllers/auth.controller.js';
import usersController from './controllers/users.controller.js';
import tracksController from './controllers/tracks.controller.js';
import adminUsersController from './controllers/admin-users.controller.js';
import adminPlaylistsController from './controllers/admin-playlists.controller.js';
import { authAdmin, authMiddleware, verifyLogin } from './authentication/auth.js';

const config = dotenv.config().parsed;

const PORT = config.PORT || 4000;
const app = express();

passport.use(jwtStrategy);
app.use(express.json());
app.use(cors());
app.use(helmet());
app.use(apiVersion);
app.use(passport.initialize());
app.use('/images', express.static('images'));

// PUBLIC ENDPOINTS
app.use('/v1/api', authController);

// PRIVATE USERS ENDPOINTS
app.use('/v1/api/users', authMiddleware, verifyLogin, usersController);
app.use('/v1/api/tracks', authMiddleware, verifyLogin, tracksController);

// PRIVATE ADMINS ENDPOINTS
app.use('/v1/api/admin/users', authMiddleware, verifyLogin, authAdmin, adminUsersController);
app.use('/v1/api/admin/playlists', authMiddleware, verifyLogin, authAdmin, adminPlaylistsController);

// NOT FOUND ERROR HANDLING
app.all('*', (_, res) => res.status(404).send({ message: 'Resource not found!' }));

app.listen(PORT, () => console.log(`RidePal app is live! Listening on port ${PORT}...`));
