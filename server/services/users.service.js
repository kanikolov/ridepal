import db from './pool.js';
import { getTrackBy, getGenreById } from '../services/tracks.service.js';
/**
 * @returns array with all users
 */
export const getAllUsers = async () => {
	const SQL = `
		SELECT id, username, email, createdOn FROM users
		WHERE isDeleted = 0 
	`;

	try {
		return await db.query(SQL);
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Searches for user in the database by field
 * @param {string} field
 * @param {string} value
 * @returns found user object
 */
export const getUserBy = async (field, value) => {
	const SQL = `
		SELECT u.id, u.username, u.email, u.createdOn, ur.role as role, u.avatar FROM users as u
		JOIN user_roles as ur ON u.roleId = ur.id
		WHERE u.isDeleted = 0
		AND u.${field} = ?
	`;

	try {
		const foundUser = await db.query(SQL, [value]);
		return foundUser[0];
	} catch (e) {
		console.log(e.message);
		return null;
	}
};

/**
 * Get all information for a user
 * @param {string} field
 * @param {string} value
 * @returns found user object
 */
export const getLoginDetailsBy = async (field, value) => {
	const SQL = `
		SELECT u.id, u.username, u.password, u.email, u.createdOn, ur.role as role FROM users as u
		JOIN user_roles as ur ON u.roleId = ur.id
		WHERE u.isDeleted = 0
		AND u.${field} = ?
	`;

	try {
		const foundUser = await db.query(SQL, [value]);
		return foundUser[0];
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Returns all playlists of a given user
 * @param {number} id user ID
 * @returns Array of objects playlists
 */
export const getUserPlaylists = async (id = null, pid = null, genres = null) => {
	const userSQL = id ? `AND userId = ${id}` : '';
	const querySQL = pid ? `AND id = ${pid}` : '';

	const SQL = `
		SELECT * FROM playlists
		WHERE isDeleted = 0
		${userSQL}
		${querySQL};
	`;

	const SQL2 = `
		SELECT trackId FROM playlists_tracks
		WHERE playlistId = ?;
	`;

	const SQL3 = `
		SELECT * FROM playlists_genres
		WHERE playlistsId = ?;
	`;

	let playlists = await db.query(SQL, [id]);

	playlists = await Promise.all(
		playlists.map(async (playlist) => {
			return {
				...playlist,
				trackList: await db
					.query(SQL2, [playlist.id])
					.then(async (data) => await Promise.all(data.map(async (track) => await getTrackBy('deezerId', track.trackId)))),
				genres: await db.query(SQL3, [playlist.id]).then(
					async (genreData) =>
						await Promise.all(
							genreData.map(async (genre) => {
								return await getGenreById(genre.genreId);
							})
						)
				),
			};
		})
	);

	if (genres)
		return playlists.filter((playlist) => {
			return playlist.genres.find((genre) => {
				return genres.includes(genre.deezerId);
			});
		});
	else return playlists;
	// p.genres.some(playlistGenre => genres.includes(playlistGenre.genreId))
};

/**
 * Update a user's playlist
 * @param {number} id userId
 * @param {number} id playlistId
 */
export const updatePlaylist = async (pid, newName) => {
	const SQL = `
		UPDATE playlists
		SET name = ?
		WHERE id = ?;
	`;

	return await db.query(SQL, [newName, pid]);
};

/**
 * Create a playlist
 * @param {number} id userId
 * @param {array} array of track Ids
 */
export const createPlaylist = async (uid, name, playtime, tracks, genres) => {
	const uniqueTracks = new Set(tracks);

	const SQL1 = `
		INSERT INTO playlists (name, totalPlaytime, userId)
		VALUES (?, ?, ?);
	`;

	const SQL2 = `
		INSERT INTO playlists_tracks (playlistId, trackId)
		VALUES (?, ?);
	`;

	const SQL3 = `
		INSERT INTO playlists_genres (playlistsId, genreId)
		VALUES (?, ?);
	`;

	const { insertId } = await db.query(SQL1, [name, playtime, uid]);

	if (insertId) {
		return await Promise.all(
			tracks.map(async (trackId) => {
				return await db.query(SQL2, [insertId, trackId]);
			}),

			genres.map(async (genreId) => {
				return await db.query(SQL3, [insertId, genreId]);
			})
		);
	}
};

/**
 * Delete a playlists
 * @param {number} id playlist id
 */
export const deletePlaylist = async (id) => {
	const SQL = `
		UPDATE playlists
		SET isDeleted = 1
		WHERE id = ?
	`;

	try {
		return await db.query(SQL, [id]);
	} catch {
		return null;
	}
};

/**
 * Updates existing user with given data inside an object
 * @param {number} id
 * @param {object} updateData
 * @returns the updated user
 */
export const updateUser = async (id, updateData) => {
	const data = Object.entries(updateData);
	const SQL = `
			UPDATE users
			SET ${data.map(([key, val]) => `${key} = "${val}"`)}
			WHERE id = ?
      		AND isDeleted = 0
    `;

	const SQL2 = `
		SELECT * FROM users
		WHERE id = ?
    	AND isDeleted = 0
	`;

	try {
		await db.query(SQL, [id]);
		const result = await db.query(SQL2, [id]);
		return result[0];
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Updates an existing user's password with new one by id
 * @param {number} id
 * @param {string} password
 */
export const updatePassword = async (id, password) => {
	const SQL = `
		UPDATE users
		SET password = ?
		WHERE id = ?
    AND isDeleted = 0
	`;

	try {
		await db.query(SQL, [password, id]);
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Updates an existing user's email with new one by id
 * @param {number} id
 * @param {string} email
 */
export const updateEmail = async (id, email) => {
	const SQL = `
		UPDATE user
		SET email = ?
		WHERE id = ?
    AND isDeleted = 0
	`;

	try {
		await db.query(SQL, [email, id]);
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Creates a user with given username and password.
 * @param {string} username
 * @param {string} password
 * @param {string} email
 * @returns {object} the newly created user
 */
export const createUser = async (username, password, email, roleId = 2) => {
	const SQL = `
		INSERT INTO users (username, password, email, roleId)
		VALUES (?, ?, ?, ?)
	`;

	try {
		const result = await db.query(SQL, [username, password, email, roleId]);
		return {
			id: result.insertId,
			username,
			email,
			roleId,
		};
	} catch (error) {
		return { error: error.message };
	}
};

/**
 * Filter users by object with query parameters
 * @param {object} queries
 * @returns array with found users
 */
export const filterUsersBy = async (queries) => {
	const SQL = `
		SELECT id, username, email, createdOn FROM users
		WHERE isDeleted = 0
		AND ${Object.entries(queries)
			.map(([q, val]) => `${q} LIKE "%${val}%"`)
			.join('\nAND ')}
	`;

	try {
		return await db.query(SQL);
	} catch (error) {
		return error.message;
	}
};
