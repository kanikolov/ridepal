import multer from 'multer';

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './images')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname.slice(file.originalname.indexOf('.')))
    }
})
const upload = multer({ storage: fileStorage });

export default upload;