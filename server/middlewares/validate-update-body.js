import errorStrings from '../common/error-strings.js';

// returns an error if there is invalid key within the request body based upon validator schema
export default (resource, validator) => (req, res, next) => {
	const errors = [];
	Object.keys(req.body).forEach((key) => {
		if (!validator[key](req.body[key])) {
			errors.push(`${key}: ${errorStrings[resource][key]}`);
		}
	});

	if (errors.length) return res.status(400).json({ errors });
	next();
};
